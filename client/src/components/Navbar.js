import React from "react";
import { Box, Text, Heading, Image, Button } from "gestalt";
import { getToken, clearToken, clearCart } from "../utils";
import { NavLink, withRouter } from "react-router-dom";
import logo from "../icons/logo.png"

class Navbar extends React.Component {
  handleSignout = () => {
    clearToken();
    clearCart();
    this.props.history.push("/shop");
  };

  render() {
    return getToken() !== null ? (
      <AuthNav handleSignout={this.handleSignout} />
    ) : (
      <UnAuthNav />
    );
  }
}

const AuthNav = ({ handleSignout }) => (
  <Box
    display="flex"
    alignItems="center"
    justifyContent="around"
    height={70}
    color="white"
    padding={1}
    shape="roundedBottom"
  >
    {/* Checkout Link */}
    <NavLink activeClassName="active" to="/checkout">
      <Box marginLeft={7} >
        <Text  size="l">
          Checkout
        </Text>
      </Box>
    </NavLink>

    {/* Title and Logo */}
    <NavLink activeClassName="active" exact to="/">
      <Box display="flex" alignItems="center">
        <Box margin={2} height={80} width={210} paddingTop={4}>
          <img
            className="vs_logo"
            alt="VS Logo"
            src={logo}
          />
        </Box>
      </Box>
    </NavLink>

    {/* Signout Button */}
    <Box marginRight={8}>
      <Button
        onClick={handleSignout}
        //color="transparent"
        text="Sign Out"
        inline
        size="sm"
      />
    </Box>
  </Box>
);

const UnAuthNav = () => (
  <Box
    display="flex"
    alignItems="center"
    justifyContent="around"
    height={70}
    color="white"
    padding={1}
    shape="roundedBottom"
  >
    {/* Sign In Link */}
    <NavLink activeClassName="active" to="/signin">
      <Text size="l">
        Sign In
      </Text>
    </NavLink>

    {/* Title and Logo */}
    <NavLink activeClassName="active" exact to="/">
    <Box display="flex" alignItems="center">
        <Box margin={4} height={80} width={200} margin={0}>
        <img
          className="vs_logo"
          alt="VS Logo"
          src={logo}
        />
        </Box>
      </Box>
    </NavLink>

    {/* Sign Up Link */}
    <NavLink activeClassName="active" to="/signup">
      <Text size="l" >
        Sign Up
      </Text>
    </NavLink>
  </Box>
);

export default withRouter(Navbar);
